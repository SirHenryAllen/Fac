20210120112725
#l2
#lc4
#c
Back to L2S4 index : [[20210125154424]]

# Langage C (L2S4)

## Amphis

[Lien BBB](https://bbb-front.math.univ-paris-diderot.fr/recherche/wie-oqi-xcn-zg0)
Wieslaw Zielonka : zielonka@irif.fr

| # | Date       | Notions                                                                                                   | #TP    |
|---|------------|-----------------------------------------------------------------------------------------------------------|--------|
| 1 | 27.01.2021 | Compile a program, basic structure, variables, booleans, conditional statements, methods, format a String | 1      |
| 2 | 03.02.2021 | enum, arrays, struct, goto                                                                                | 2      |
| 3 | 10.02.2021 | pointers                                                                                                  | 3      |
| 4 | 17.02.2021 | pointers (suites)                                                                                         | 4      |
| 5 | 03.03.2021 | pointeurs sur les structures                                                                              | 4 et 5 |
| 6 | 10.03.2021 | string, strlen, strcopy                                                                                   | 6      |
| 7 | 17.03.2021 | string, strcat                                                                                            | 7      |

Notes d'amphi :
- 1 : [[20210127164056]]
- 2 : [[20210210155657]]
- 3 : [[20210210161830]]
- 4 : [[20210217154320]]


## TP

Sur Discord
Leonard Guetta : guetta@irif.fr

| # | Date       | Finished | Notions                                                                                     | Amphi # |
|---|------------|----------|---------------------------------------------------------------------------------------------|---------|
| 1 | 01.02.2021 | x        | basic program, compiling, basic methods, loops, variables, arrays                           | 1       |
| 2 | 08.02.2021 | x        | tableaux, structures                                                                        | 2       |
| 3 | 15.02.2021 | x        | tri à bulles, pointeurs (rien de bien compliqué)                                            | 3       |
| 4 |            | x        |                                                                                             |         |
| 5 | 08.03.2021 | x        | pointeurs sur les structures, différences entre adresses mémoires sur le tas ou sur la pile | 4 et 5  |
| 6 | 15.03.2021 |          | manipulations de string                                                                     | 6       |


## Ressources

### Links

- [Openclassrooms class](https://openclassrooms.com/fr/courses/19980-apprenez-a-programmer-en-c)
- [Youtube playlist](https://www.youtube.com/playlist?list=PLrSOXFDHBtfEh6PCE39HERGgbbaIHhy4j)


### Books
- Peter Von Der Linden  : Expert C programming. Deep C secrets
