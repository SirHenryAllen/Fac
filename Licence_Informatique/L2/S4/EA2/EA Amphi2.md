20210203134719
#l2
#ea4
#algo
Back to L2S4 index : [[20210120111811]]

# EA Amphi 2

## Complexité

### En espace

- quantité de mémoire nécessaire pour effectuer le calcul

- la **mémoire incompressible** : celle qui stocke les données et le résultat
    - on s'en fiche de celle la vu qu'on ne peut pas la réduire
    - tous les algos ont la même

- la **mémoire auxiliaire** : pour les calculs intermédiaires

## Notations

### Grand O

![c233c9e12176bffa341bf2aacb7b4c96.png](pictures/c233c9e12176bffa341bf2aacb7b4c96.png)
![464f55d573cf408b11b9d0ba04e64bec.png](pictures/464f55d573cf408b11b9d0ba04e64bec.png)

### Oméga

![3d2fb95473941b523755c6edd504ac1d.png](pictures/3d2fb95473941b523755c6edd504ac1d.png)

![2ea2dfe2ed22082f103b296e13676c35.png](pictures/2ea2dfe2ed22082f103b296e13676c35.png)


### Théta

![7163aa25237dd8cda104b0af3989771b.png](pictures/7163aa25237dd8cda104b0af3989771b.png)
![6b72acdfe6579860f189a5cbee39cd99.png](pictures/6b72acdfe6579860f189a5cbee39cd99.png)
