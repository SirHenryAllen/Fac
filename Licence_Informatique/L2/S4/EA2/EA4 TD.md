20210201083427
#l2
#ea4
#algo

Back to EA index : [[20210120111811]]


# EA4  : TD

## TD 1 : Opérations arithmétiques

| # | OK | Notions                                                   | Difficulté |
|---|----|-----------------------------------------------------------|------------|
| 1 | x  | temps de calcul, complexité                               | facile     |
| 2 | x  | exécuter un algo, preuve de correction, base 2            | moyenne    |
| 3 | x  | exponentiation rapide                                     | dur        |
| 4 | x  | méthode d'Horner, temps de calcul, exponentiation bianire | facile     |
| 5 | x  | nombres premiers, crible d'eratosthène                    | moyenne    |


## TD2 : Calculs de complexité

| # | OK | Notions                                         | Difficulté |
|---|----|-------------------------------------------------|------------|
| 1 | x  | notations asymptotiques (grand O, oméga, théta) | moyenne    |
| 2 | x  | Karatsuba                                       | facile     |
| 3 | x  | Complexité d'algo récursifs                     | moyenne    |
| 4 |    |                                                 |            |
| 5 |    |                                                 |            |


## TD3 : Complexité et récursivité

| # | OK | Notions                             | Difficulté |
|---|----|-------------------------------------|------------|
| 1 |    | Complexité, classement de fonctions | facile     |
| 2 |    |                                     |            |
| 3 |    |                                     |            |

