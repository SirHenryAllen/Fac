20210120111811
#l2
#ea4
#algo

Back to L2S4 index : [[20210125154424]]

# Elements d'algorithmique (L2S4)

## Amphis

Dominique Poulalhon : dominique.poulalhon@irif.fr
Toujours mettre [EA4] dans le sujet des mails.
[Lien BBB](https://bbb-front.math.univ-paris-diderot.fr/recherche/dom-mf9-g7l-oaj)
code d'accès 640747

| # | Date       | Notions                                                                                           | TD # | TP # |
|---|------------|---------------------------------------------------------------------------------------------------|------|------|
| 1 | 27.01.2021 | bases de python, algos en python sur les opérations élémentaires sur les entiers (sur les slides) | 1    | 1    |
| 2 | 03.02.2021 | complexité, grand O, théta, oméga                                                                 | 2    | 2    |
| 3 | 10.02.2021 | suite de la complexité, algo de Karatsuba pour la multiplication                                  | 3    | none |
| 4 | 17.02.2021 | tri de tableau/listes, comparaisons entre tableaux et listes pour différents algos                | 4    | 3    |
| 6 | 10.03.2021 | quicksort, tri par insertion, maths sur les permutations                                          | 5    | 3    |

Links to amphis notes :
- 1 : [[20210127132937]]
- 2 : [[20210203134719]]


## TD

Maxime Bombar : maxime.bombar@inria.fr
Sur Discord

Links to TD notes : [[20210201083427]]

## TP

Sur Discord, les profs font une permanence le jeudi


## Ressources

- in the coursEA42019-2020 : the material for the class on the previous year


## MCC

Contrôle continu intégral.

- 50% contrôle final (sur table si possible)
- on essaiera de faire un partiel aussi
- des notes obtenues pdt le semestre
- un note de participation (présence en TD et TP, rendu des TP)
