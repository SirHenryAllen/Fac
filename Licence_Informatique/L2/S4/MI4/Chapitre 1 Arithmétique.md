20210127113053
#l2
#maths
#mi4
#arithmetic
Back to MI4 index : [[20210120112825]]

# Chapitre 1 : Arithmétique

**amphi 1 : 27.01.2021**

## Divisibilité

### Définition

- définition de la divisibilité pour les entiers
- propositions
        - transitivité ?
        - si a|b et b|a ?
        - si a|b et a|c, que dire de kb + lc ?
- définition des nombres premiers

### Division euclidienne

- définition 
- démonstration
- extension aux entiers relatifs.  A quoi faut-il faire attention ?

### Ecriture en base N

- théorème
- démonstration de l'existence

### PGCD, PPCM

- définition de diviseur et multiple commun
- montrer que l'ensemble des diviseurs communs de a et b est fini
- montrer que l'ensemble des multiples communs de a et b a un plus petit élément
- PGCD(a,0) = ?
- PGDC(0,0) = ?
- définition nombres premiers entre eux
- soient a et b tels que PGCD(a,b) = d. Comment trouver des nombres premiers entre eux à partir de ça ?
- démonstration du lemme ci-dessus

### Calcul du PGCD et algorithme d'Euclide

- lemme pour calculer le PGCD(a,b) à partir de la division euclidienne de a par b
- démonstration de ce lemme

**amphi 2 : 03.02.2021**

- algorithme d'Euclide

### Théorème de Bezout, Lemmes de Gauss et d'Euclide

- théorème
- donner une caractérisation de deux nombres premiers entre eux avec le théorème de Bezout
- que peut on donner comme relation entre un diviseur c de a et b, et le PGCD(a,b) ?
- que dire de PGCD(ca, cb) ?
- lemme de Gauss
- lemme d'Euclide
- si a|c et b|c et PGCD(a,b) = 1, que dire de c et ab ?
- PGCD(a,b) * PPCM(a,b) = ?
- si a et b sont premiers entre eux, PPCM(a,b) = ?


**amphi 3 : 10.02.2021**

### Théorème fondamental de l'arithmétique

- théorème
- démonstration
- démonstration qu'il y a une infinité de nombres premiers


### Application de Bezout à la résolution d'équations

- expliquer la méthode

**amphi 4 : 17.02.2021**

## Congruences

### Définition
- définition
- propriétés, si a cong b mod n et c cong d mod n
    - a + c et b +d ?
    - a - c et b - d ?
    - ac et bd ?
    - a + b et c + d ?
- prop : si ab cong = ac mod n, alors b et c ?
- y'a t'il une solution x de ax cong b mod n
    - si a et n sont premiers entre eux ?
    - si d = PGCD(a, n) ?

**amphi 5 : 03.03.2021**

### Théorème des restes chinois
- théorème
- et si n et m ne sont pas premiers ?

### **Z**/n**Z**

- définition







