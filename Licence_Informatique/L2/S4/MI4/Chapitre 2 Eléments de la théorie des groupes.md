20210303105219
#l2
#maths
#mi4
#arithmetic
Back to MI4 index : [[20210120112825]]

# Chapitre 2 : Eléments de la théorie des groupes

**amphi 5 : 02.03.2021**
## Groupe

### Définition
- définition d'un groupe
- les axiomes d'un groupe sont ?
- définition de la loi de composition d'un groupe
- définition d'un groupe fini

### Sous-groupes
- définition d'un sous-groupe
- sous-groupe engendré par un ensemble
- définition d'un groupe cyclique
- Z/nZ engendré par k à quelle condition ?

**amphi 6 : 10.03.2021**

### Théorème de Lagrange
- théorème
- preuve
- à quelle condition sur l'ordre G est-il cyclique ?
- qu'est ce que l'ordre d'un élément de G ?
- définition de l'ordre infini pour un élément x ?
- si x^n = élément neutre, que peut-on dire de la divisibilité de n ?
- vrai ou faux : dans un groupe fini, il peut y avoir des éléments d'ordre infini 
- si G un groupe fini et x dans G, quelle relation entre l'ordre de x et l'ordre de G ?

**amphi 7 : 17.03.2021**

### Applications à l'arithmétique

- définition de l'ordre multiplicatif pour un élément dans Z/nZ inversible
- caractéristique d'Euler et lien avec (Z/nZ)^étoile
- théorème d'Euler
- petit théorème de Fermat
- si a et b sont premiers entre eux, que dire de phi(ab) ?

### Homomorphisme et isomorphisme
- définition d'un homomorphisme
- si f un homomorphisme de (G, .) dans (H, \*) :
    - que dire de f(neutre de G) ?
    - que dire de f(x^-1) ?
- image d'un morphisme
- noyau d'un morphisme
- un morphisme est injectif ssi quelle prop sur son noyau ?
- que dire de Im(f) et Ker(f) en tant que groupes ?

**amphi 8 : 24.03.2021**

###  Permutations

#### Groupe symmétrique
- definition du n-ième groupe symétrique
    - quel est l'inverse d'un élément f dans un tel groupe ?
- def d'une permutation 
- combien Sn a-t-il d'éléments ?

#### k-cycle
- défnition d'un k-cycle
- comment appelle-t-on un 2-cycle ?
- qu'est ce que le support d'un k-cycle ? 
- quel est l'ordre d'un k-cycle ?
- à quelle condition deux cycles commutent-ils ?

**amphi 9 : 31.03.2021**

#### transpositions, signature d'une permutation
- donner un ensemble générateur de Sn
- définition d'une inversion
- définition de la signature d'une permutation
- quand est ce qu'une permutation est paire ? impaire ?
- combien vaut la signature d'une transposition ?
- combien vaut la signature du produit de 2 permutations ? et de la somme ?
- définition du groupe alterné
- combien de permutations paires dans Sn ?
- est ce que l'ensemble des permutations paires est un groupe ? et les permutations inpaires ?
- que vaut l'ensemble des permutations paires par rapport à Sn ?
- combien vaut la signature d'une permutation ? comment la calculer facilement ?

#### orbite
- définition de l'orbite de x ?
- quand est ce qu'une permutation est un k-cycle ? (en fonction de ses orbites)

**amphi 10 : 07.04.2021**

#### décomposition
- peut-on tjrs décomposer une permutation en :
    - cycles à supports joints ?
    - cycles à supports disjoints ?
    - transpositions ?

- peut-on facilemement calculer la signature à partir de la décomposition en :
    - cycles à supports disjoints ?
    - cycles à supports joints ?
    - transpositions ?

- quel est l'ordre d'un k-cycle ?
- comment calculer l'ordre d'un produit de k-cycles disjoints ?