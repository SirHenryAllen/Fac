20210407112230
#l2
#maths
#mi4
#arithmetic
Back to MI4 index : [[20210120112825]]

# Chapitre 3 : Probabilités

**amphi 10 : 07.04.2021**

## Notions de base

### Définitions
- univers
- événement
- événement élémentaire
- événement réalisé
- donner un événement impossible et un événement certain
- tribu
- loi de probabilité
- espace probabilisé

### Notations ensemblistes
- A implique B
- A ou B
- A et B
- contraire de A
- A et B sont incompatibles

### Formules de base
- P(A barre) ?
- P(ensemble vide) ?
- P(A U B) si on ne suppose pas que A et B sont incompatibles ?
- Si A inclu dans B, que dire de P(A) et de P(B) ?

**amphi 11 : 14.04.2021**

## Combinatoire
- quels sont les 4 types de tirage ?
- tirer k éléments parmis n éléments : combien d'arragement possiblesz ?
    - avec remise et ordre compte
    - avec remise et ordre ne compte pas
    - sans remise, ordre ne compte pas
    - sans remise, ordre compte
