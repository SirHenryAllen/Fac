20210120112825
#l2
#mi4
#maths
Back to L2S4 index : [[20210125154424]]

# Index for the Mathematics class (L2S4)

## Amphis

Adrien Brochier :  adrien.brochier@imj-prg.fr
[Lien BBB](https://bbb-front.math.univ-paris-diderot.fr/recherche/adr-m0f-k4o-ogp)


## Lien vers les fiches de résumé des notions :

[[20210127113053]] Chapitre 1 Arithmétique
[[20210303105219]] Chapitre 2 Eléments de la théorie des groupes
[[20210407112230]] Chapitre 3 Probas

## TD

Bernhard Keller : bernhard.keller@imj-prg.fr
[Lien Zoom](https://u-paris.zoom.us/j/82517013339?pwd=YTFVQmgxVFpTNkJUVmMwQjBaV0hqZz09#success)
[corrections du prof](https://cloud.math.univ-paris-diderot.fr/s/ZW6pGSfGYJjHAsD)

Numéro de réunion : 825 1701 3339
Mot de passe : 736574

[Correction des TD](http://abrochier.org/enseignement.php )

Liens vers l'avancement des TD : 
- 1 : [[20210128134708]]
- 2 : [[20210316150309]]


 