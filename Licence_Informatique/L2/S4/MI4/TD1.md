20210128134708
#l2
#maths
#mi4
#arithmetic
Notes du cours du chapitre : [[20210127113053]]
Back to MI4 index : [[20210120112825]]

# TD1 : Arithmétique dans Z

| OK | #  | Notions                                               | Correction | Difficulté      |
|----|----|-------------------------------------------------------|------------|-----------------|
| x  | 1  | divisibilité, théorème  fondamental de l'arithmétique | x          | easy            |
| x  | 2  | divisibilité, théorème  fondamental de l'arithmétique | x          | easy            |
| x  | 3  | division euclidienne, congruences                     | x          | easy            |
| x  | 4  | division euclidienne                                  | x          | easy            |
| x  | 5  | divisibilité, Bezout, Euclide                         | x          | medium but long |
|    | 6  | divisibilité                                          | x          | easy            |
|    | 7  | divisibilité                                          | x          | easy            |
| x  | 8  | bases                                                 | x          | easy            |
| x  | 9  | bases                                                 | x          | easy            |
| x  | 10 | bases                                                 | x          | easy            |
|    | 11 | facteurs premiers, divisibilité                       | x          | medium          |
|    | 12 | bases                                                 | x          | medium          |
| x  | 13 | PGCD, PPCM                                            | x          | facile          |
|    | 14 | nombres premiers, PGCD                                | x          | medium          |
|    | 15 | PGCD                                                  |            |                 |
|    | 16 | PGCD, PPCM                                            | x          | medium          |
|    | 17 | PGCD, Bezout                                          | x          | medium          |
|    | 18 | Bezout                                                | x          | medium (long)   |
|    | 19 | PGCD, PPCM                                            |            |                 |
|    | 20 | PGCD                                                  |            |                 |
|    | 21 | Bezout                                                |            |                 |
|    | 22 | nombres premiers                                      |            |                 |
|    | 23 | nombres premiers                                      |            |                 |
|    | 24 | nombres premiers entre eux                            |            |                 |
|    | 25 | bases, nombres premiers                               |            |                 |
|    | 26 |                                                       |            |                 |
|    | 27 |                                                       |            |                 |


