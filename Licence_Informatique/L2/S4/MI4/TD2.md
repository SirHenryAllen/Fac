20210316150309
#l2
#maths
#mi4
#arithmetic
Notes du cours du chapitre : [[20210127113053]]
Back to MI4 index : [[20210120112825]]

# TD2 : Congruences

| OK | #  | Notions                                           | Difficulté | Correction |
|----|----|---------------------------------------------------|------------|------------|
| x  | 1  | congruences, division euclidienne                 | easy       | x          |
| x  | 2  | table de congruence                               | easy       | x          |
| x  | 3  | table de congruence                               | easy       | x          |
| x  | 4  | résolution de système avec 2 congruences          | medium     | x          |
| x  | 5  | résolution de système avec 2 congruences          | medium     | x          |
| x  | 6  | PGCD, nombres premiers entre eux                  | easy       | x          |
|   | 7  | ?                                                 |            |            |
| x  | 8  | divisibilité et congruences                       | medium     | x          |
| x  | 9  | sommes, congruences, divisibilité                 | medium     | x          |
| x  | 10 | reste dans la division euclidienne de gds nombres | easy       | x          |
| x  | 11 | montrer une congruence                            |            |            |
| x  | 12 | résoudre des équations ax = b mod c               |            |            |
| x  | 13 | classes de congruences                            |            |            |

