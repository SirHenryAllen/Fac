20210120112842
#l2
#ol4
#logic
Back to L2S4 index : [[20210125154424]]

# Outils Logiques (L2S4)

## CMTD

| # | Date       | Zettlr | Notions                                                                                          | dans le syllabus | pdf     | Vidéos (Moodle) |
|---|------------|--------|--------------------------------------------------------------------------------------------------|------------------|---------|-----------------|
| 1 | 01.02.2021 | x      | relations, réécritures, ordres bien fondés, terminaison                                          |                  | cours 2 | 4 (partie 2)    |
| 2 | 08.02.2021 |        | signature, logique prépositionnelle                                                              |                  | cours 1 | 4 (partie 1)    |
| 3 | 15.02.2021 | x      | syntaxe et sémantique du calcul propositionnel; validité, satisfaisabilité et équivalent logique |                  | cours 3 | 3 (partie 3)    |
| 4 | 01.03.2021 | x      | équivalence logique, fonction définissable, forme normale (conjonctive et disjonctive)           |                  | cours 3 | 3 (partie 4)    |

[https://drive.google.com/drive/folders/15pKXn2oX946XJp8CQlgG-u6xLHzkUGfr?usp=sharing](https://drive.google.com/drive/folders/15pKXn2oX946XJp8CQlgG-u6xLHzkUGfr?usp=sharing)

Links to notes :
- 1 : [[20210201132703]]
- 2 : [[20210205154314]]
- 3 : [[20210228183127]]

## DM

| # | Due Date   | Turned in  | Notions                                                             | CMTD# |
|---|------------|------------|---------------------------------------------------------------------|-------|
| 1 | 08.01.2021 | 07.02.2021 | relations, réécritures, ordres bien fondés, terminaison             | 1     |
| 2 | 15.02.2021 | 15.02.2021 | signatures, algèbre initiale, morphisme, terminaison d'une relation | 2     |
| 3 | 01.03.2021 | 27.02.2021 | calculs propositionnels, taille, substitution                       | 3     |


## Notes

Tout en controle continu : 
50% = moyenne de tous les CC sauf le dernier
50% = la note du dernier CC

Epreuve de rattrapage : remplace la moins bonne note entre le dernier CC et la moyenne des autres CC

## Ressources

- a pdf with an outline of the class, written by the teacher

