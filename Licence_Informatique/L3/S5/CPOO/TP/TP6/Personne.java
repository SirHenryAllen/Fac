public class Personne {
	private final String name;

	public Personne(String name) {
		this.name = name;
	}

	public String toString() {
		return "Je suis la " + name;
	}
}