# LO5 Index page
#logic
#propositional_logic 
#first-order_logic

## Ressources

- [The teacher's page](https://www.youtube.com/channel/UCawSURMSd5Ll8I0DXv1CTHA)
- [Yt videos](https://www.youtube.com/channel/UCawSURMSd5Ll8I0DXv1CTHA) by the teacher (filmed during lockdown last year, the first few are crap then it gets better)
- A pdf of notes (on the teacher's page)
- [A Moodle page](https://moodle.u-paris.fr/course/view.php?id=1657)

## Context
Logic is very useful in computer science :

- [[Curry-Howard theorem]] => formal link between computer programs and logical proofs. You can write a formal specification of a program to make sure it behaves as intended.

- [[Logical circuits]] => physical implementation of logical formulas. Logic is therefore useful to determine whether 2 circuits are equivalent, what a circuit does, if it can be minimized (less components on the board = more profit).

- Algorithmic complexity => logical problems are "difficult" problems (eg : [[SAT]] = the satsifiability of a formula).
- Lots of real life problems can be reduced to logical formulas. [[SAT solvers]] are programs that resolve a lot of them. It is important that they are as efficient as possible.
- [[Logical programming]] => a program that resolves one of those problems must resolve a set of constraints. Languages like Prolog or Mozart/Oz let the dev set the constrains and then find the values that satisfy it.

- [[Codd theorem]] => links the operations you can perform on a database ([[Relational algebra]]) with the queries you can write (relational calculs = [[First-order logic]]). Logic can help to optimize the queries.


## Propositional calculus

[[Propositional calculus]]



## First-order logic

