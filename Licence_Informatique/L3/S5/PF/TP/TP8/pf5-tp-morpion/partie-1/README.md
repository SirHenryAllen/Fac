
TP sur le jeu de morpion, partie 1
==================================

## Morpion simple

1. Sur la plateforme LearnOcaml de pf5, faire les exercices
   - 8.1 Morpion (début)
   - 8.2 Morpion (condition de victoire)

2. Quand vos solutions sont valides, les télécharger dans ce répertoire
   en gardant les noms proposés `8.1_morpion_debut.ml` et `8.2_morpion_victoire.ml`

3. Compiler et lancer le morpion via : `./morpion.sh`

Ceci nécessite d'avoir installé localement `ocaml`, `dune` et `graphics`,
voir [INSTALL.md](https://gaufre.informatique.univ-paris-diderot.fr/letouzey/pf5/blob/master/INSTALL.md).


## Méta-morpion

1. Sur la plateforme LearnOcaml de pf5, faire les exercices
   - 8.3 Méta-Morpion (début)
   - 8.4 Méta-Morpion (condition de victoire)

2. Quand vos solutions sont valides, les télécharger dans ce répertoire
   en gardant les noms proposés `8.3_metamorpion_debut.ml` et
   `8.4_metamorpion_victoire.ml`. Les solutions précédentes au morpion
   simple doivent également toujours être dans ce répertoire.

3. Compiler et lancer le morpion via : `./metamorpion.sh`
