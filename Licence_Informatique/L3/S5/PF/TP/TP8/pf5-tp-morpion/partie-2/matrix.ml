type 'a t = 'a array array

(* Créée une matrice carrée de taille n,
   dont l'élément à la position (x,y) est f (x,y) *)
let init n f = failwith "TODO"

(* Renvoie l'élément de la matrice à la position (x,y) *)
let get matrice (x,y) = failwith "TODO"

(* Renvoie la taille de la matrice *)
let size matrice = failwith "TODO"

(* Change la valeur de l'élément de matrice à la position (x,y) en v *)
let set matrice (x,y) v = failwith "TODO"
