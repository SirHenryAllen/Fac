## Liste des équipes de projet

Sauf mention contraire, j'ai bien trouvé un unique dépôt, privé, qui est
bien un fork du projet.

Les équipes taguées (\*) ont un dépôt mais ne se sont pas déclarées. Merci de me confirmer la constitution de votre équipe par mail.

### Équipe 1
- CHALUMEAU Maxime 
- MATHIEU-BLOISE Benjamin 
- SIMONOT Manon 

### Équipe 2
- BA Ibrahima
- CHHAY Davy
- SILVESTRE Alaia

### Équipe 3
- COURTIN Jules 
- DERRI Maxime 
- POURNY Louis 

### Équipe 4
- ALLICHE Zahra
- KHALAL Noor
- KHAN Abdullah

### Équipe 5
- FOURMONT Baptiste 
- MANYIM Olivier 
- SIMONEAU Louis

### Équipe 6
- GOLIKOVA Anna
- MAUDET Benjamin
- TEIXEIRA MEDEIROS Claudio

### Équipe 7
- GANGNEUX Paul
- MOREAU Olivier
- OLIVIER Lily

### Équipe 8
- AFONSO PECADO Mathieu
- JOKSIMOVIC Jasmin
- NGUYEN Emilie Thuy-Vi

### Équipe 9
- HUANG Yingqi
- LI Songqiao
- YE Junli

### Équipe 10
- BELAGE Tom 
- DENIS Marine 
- LÉVÊQUE Élodie 

### Équipe 11
- BOUIZEGARENE Damya
- WALLISER Marie
- ZIDAT Lydia

### Équipe 12
- CINTERO François 
- LE CORRE Léo 
- QUACH Kévin 

### Équipe 13
- GUINET Virgile
- TAZOUEV Arbi
- VAIN Alexandre

### Équipe 14
- BADAOUI Ismaïl
- BENAZIZA Chems-Eddine 
- VERHAEGHE Jean-Yves

### Équipe 15
- AFONSO Cassandra
- JIN Jacqueline
- MORAND Paul-Émile

### Équipe 16
- AOUINE Katia
- MTIR Asma
- NEDJAÏ Étienne

### Équipe 17
- ADAM Pauline
- RUNSER Laure 
- TORIS Ugo 

### Équipe 18
- MAZELET Florent
- OULDBELKACEM Nael
- ZENKRI Jihed

### Équipe 19
- CUNHA TEIXEIRA José Miguel 
- KALOUSDIAN Leah 
- ROCHIER Marie 

### Équipe 20
- DUPONT-LATAPIE Ulysse 
- JAULMES Bérénice
- HAMON Youri

### Équipe 21
- MIKIA Benidy
- NASR Amira
- NODIN Aurélie

### Équipe 22
- BENAKLI William
- JACOTOT Hugo
- LE FRANC Matthieu

### Équipe 23
- BRAGINA Natalia
- SAAD David
- TEIXEIRA Gabriel

### Équipe 24
- ALEXANDRE DOS SANTOS Dylan
- DERVISHI Sevi
- PAYET Jylan
**(attention, projet public)**

### Équipe 25
- PREVERT Alva
- XU Cécile
- ZHANG Sébastien

### Équipe 26
- JEAN MARIE Roude 
- POULARD Rémi
- SOUFIR Emma

### Équipe 27
- BELHOUT Chaima
- ROMELE Gaspard
- VIGNESWARAN Tharsiya

### Équipe 28
- BOTTI Emma 
- NGOY John
- SAILLY Steven

### Équipe 29
- AMIRI Aziz
- FLEURANT Martin
- MADIVANANE Rémi

### Équipe 30
- AGGOUN Tara
- JEDDI Skander
- TANG Elody

### Équipe 31
- DONI Okpê Diane
- LUONG Vi Long
- ZHANG Maryline

### Équipe 32
- SHAN Michel
- TAGHAYOR Ellenor Fatemeh
- ZHANG Xujia
*(merci de supprimer le dépôt en doublon inutilisé)*

### Équipe 33
- CHEMEQUE Maël
- CHIKHI Cylia
- SAHI Kenza

### Équipe 34
- FOUQUET Aurélie 
- SY Alassane
- TAN Julia

### Équipe 35
- BENNACEUR Ryan
- LÉVY Rémi
- RAVENEAU-GRISARD Émilien

### Équipe 36
- BENDINI Aya
- BOUDJABOUT Manissa
- KACETE Zuina

### Équipe 37
- CHERION Jules
- LEYMARIE Alexandre
- ROUGEOLLE Yoan

### Équipe 38
- IMMOUNE Ghillas
- KHALI Mehdi
- RODRIGUES Simon

### Équipe 39
- KHILA Wissal
- NGUYEN TUONG Léo
- VERDIER Thomas

### Équipe 40
- AL AZAWI Rayan
- HAMIMI Dany
- KAABECHE Rayane

### Équipe 41
- BENNASSER Adib
- BOUDAOUD Kenza
- TRAN An-Tiêm

### Équipe 42
- CRANE Sophie
- YEUTSEYEVA Sasha
- 

### Équipe 43
- AMMICHE Naïma
- COHEN Chlomite
- NOUALI Noura

### Équipe 44
- BOUANEM Yani Akli
- LE Florian
- SOMON Bastian 

### Équipe 45
- AZIZ Ghizlane
- MOKEDDES Asma
- TOUAZI Ryad Aimen

### Équipe 46
- CABALLERO AQUINO Leticia
- FUENTES Dorian
- TRAN Marilyn

### Équipe 47
- DJELJELI Yassine
- SAMSON Pierre
- ZHANG Changrui

### Équipe 48
- BLED Nolan
- KELLER Dylan
- YE Nicolas

### Équipe 49
- HABLAL Abdelhamid
- TOUKAL Raouf
- 

### Équipe 50
- ANDRIANANTENAINA Maminiaina Tokinomena
- JOSSO Victor
- KURDYK Louis

### Équipe 51
- LOUNES Ismaël
- SEDDIKI Bilal
- 

### Équipe 52
- HADDACHE Maya 
- HAMMOUCHE Mohamed Aimen
- MORON USON Guillermo

### Équipe 53
- DOROSENCO Nadejda
- HOU Maxime
- MORRISSON Kevin
*(attention, projet public)*

### Équipe 54
- JAUROYON Maxime
- KINDEL Hugo
- 

### Équipe 55
- BELARIF Farid
- ELHAZ Nadine
- MACIA Lua


### Équipe 56 (\*)
- BAH Awa
- DJELJELI Yassine
- GUINARD Océane

### Équipe 57
- HARROUG Amina
- MEZIANE Ahmed-Massinissa
- MUKAILA ALH SHITTU Abdoul Basti

### Équipe 58
- DAI Anna
- FRANCOIS Tanya
- RABEHANTA Line Niryantso

### Équipe 59 (\*)
- AIT SAID Celina
- BACHA Rayane
- GUEDDA Mohamed

### Équipe 60 (\*)
- BENAMEUR Abdesselam
- IGUENI Hakim
- LOUNI Narimane

### Équipe 61 (\*)
- CAI Thierry
- CHENG Daniel
- VELANGANNI Jean-Paul

### Équipe 62
- BECHBECHE Mahieddine
- ROVERI Alessio
- SOUMARE Cheikou

### Équipe 63 (\*)
- OUTAYEB Kenza
- RACHI Adel
-

### Équipe 64 (\*)
- MAUFFRET Théo
- SONNEVILLE Charly
-

### Équipe 65 (\*)
- JAMI Adam
- SCHEINER Ymri
-

### Équipe 66
- HADJLOUM Massi
- HELAL Imad Eddine
- YAHIAOUI Halim

### Équipe 67
- ESSAHLAOUI Mouadh
- TAGELDIN IBRAHIM Maaz
- ZHOU Cédric

### Équipe 68
- CHALAL Massinissa
- HAMITOUCHE boukhalfa
- SMAINI Khalida

### Équipe 69
- ABDELLATIF Kenzi
- HANNA Destiny
- SIMONNET William
**(attention, les enseignants sont seulement guests)**

### Équipe 70 (\*)
- HAFID Yahya
- OUBADIA Tanel
-

### Équipe 71
- HURET Basile
- LE JEUNE Alban

