# TD1

## Exercice 1

**1).** L'acteur du système est le **client*, il commande le système via l'intermédiaire de la gâchette.

**2).** On peu modéliser le cas où "Bob" est pompiste et se sert de l'essence (comme le ferais un client) en étendant le cas d'utilisation.

**3).** Lorsque Bob vient avec son camion pour remplir les RESERVOIRS DES POMPES, il est considéré comme un nouvel acteur, il est "pompiste".

**4).** Les pompistes chargés de la maintenance sont des "opérateurs", un rôle étendant celui de "pompiste".

## Exercice 2

Le client peut commander sur place, retires la marchandise, saisir un code client pour procéder au paiement.