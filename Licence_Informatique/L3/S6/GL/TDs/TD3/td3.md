# TD3

## Exercice 1

**Acteurs :**

* **1).** Arrivée en caisse du client
* **2).** le caissier liste les produits
* **6).** le caissier propose la fin de la vente
* **7).** le client accepte et choisit le mode de paiement
* **9).** *(optionnel)* rendre le ticket
* **10).** le client part


**Système :**

* **3).** *(optionnel)* affichage de la liste des produits et de leur prix
* **4).** calcul du total des prix
* **5).** affichage du total
* **8).** enregistrer le paiement et rendre le ticket
