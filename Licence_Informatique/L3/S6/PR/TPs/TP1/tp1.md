# Exercice 2:
	1/. **hostname -d** : le nom de domaine
		**hostname -i** : l'ip du réseau (routeur) 
	2/. **ping www**, ping la machine du réseau (de l'UFR) ?
	3/. **ping www**, on ping la machine du réseau sur lequel on est connecté
		**ping lulu**, on ping la machine du sous-réseau
	4/. **www.free.fr**, 
		**www.informatique.univ-paris-diderot.fr**, trotinette
	5/. **dig www.informatique.univ-paris-diderot.fr MX**, korolev, potemkin, shiva
		**dig www.free.fr MX**, freens1-g20.free.fr
	
# Exercice 3:
	1/. cat /etc/services | grep discard -> 9/tcp v 9/udp
	2/. telnet lulu 13
		telnet lulu daytime

# Exercice 4:
	1/. 