# Informations

**NOM :** Le Franc

**Prénom :** Matthieu

**Groupe :** 4

**Mail :** matthieu.le-franc@etu.u-paris.fr

---

# Pour compiler/executer

* lancer la commande **make** dans le répertoire
    * cela compilera tous les fichiers.c
* exécuter successivement les commandes
    * **./serveur *num_port***
    * **./client1 *num_port***
    * **./client2 *num_port***
