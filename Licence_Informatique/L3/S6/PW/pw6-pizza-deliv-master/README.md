# pw6 pizza deliv

## Gestion base de donnée

Commandes à exécuter dans le terminal

* **Création de l'utilisateur :** CREATE USER test with encrypted password 'test' ;
* **Création de la database :** CREATE DATABASE test ;
	
**Pour lancer le serveur :** node main.js

**Pour accéder au site :**

* localhost:8080 *(en local, après avoir suivi la procédure ci-dessus)*
* mlefranc.com:8080 *(pour accéder au site hébergé sans avoir à réaliser la configuration préalable)*